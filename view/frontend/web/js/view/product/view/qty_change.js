define([
    'ko',
    'uiComponent'
], function (ko, Component) {
    'use strict';
    return Component.extend({
        initialize: function () {
            //initialize parent Component
            this._super();
            this.qty = ko.observable(this.defaultQty);
            this.qty_increments = Math.round(this.QtyIncrements);
        },
        decreaseQty: function() {
            var newQty = this.qty() - this.qty_increments;
            if (newQty < this.qty_increments)
            {
                newQty = this.qty_increments;
            }
            this.qty(newQty);
        },
        increaseQty: function() {
            var newQty = this.qty() + this.qty_increments;
            this.qty(newQty);
        }
    });
});
